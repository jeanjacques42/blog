<?php

namespace App\Repository;

use App\Entity\Article;

class BlogRepository
{

    public function findAll(): array
    {
        $articles = [];


        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("SELECT * FROM article");

        $query->execute();

        $results = $query->fetchAll();
        foreach ($results as $line) {
            $articles[] = $this->sqlToBlog($line);
        }
        return $articles;
    }

    public function add(Article $article): void
    {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("INSERT INTO article (id, titre, descript) VALUES (:id, :titre, :descript)");
        $query->bindValue(":id", $article->id, \PDO::PARAM_INT);
        $query->bindValue(":titre", $article->titre, \PDO::PARAM_STR);
        $query->bindValue(":descript", $article->descript, \PDO::PARAM_STR);
        $query->execute();

        $article->id = $connection->lastInsertId();
    }

    public function find(int $id): ? article
    {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("SELECT * FROM article WHERE id=:id");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();

        if ($line = $query->fetch()) {
            return $this->sqlToBlog($line);
        }
        return null;
    }

    public function update(Article $article): void
    {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("UPDATE article SET titre=:titre, descript=:descript WHERE id=:id");
        $query->bindValue(":titre", $article->titre, \PDO::PARAM_STR);
        $query->bindValue(":descript", $article->descript, \PDO::PARAM_STR);
        $query->bindValue(":id", $article->id, \PDO::PARAM_INT);

        $query->execute();
    }

    public function remove(Article $article): void
    {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("DELETE FROM article WHERE id=:id");
        $query->bindValue(":id", $article->id, \PDO::PARAM_INT);

        $query->execute();
    }
    private function sqlToBlog(array $line): Article
    {
        $article = new Article();
        $article->id = intval($line["id"]);
        $article->titre = $line["titre"];
        $article->descript = $line["descript"];
        return $article;
    }

    public function random(array $array): ? Article
    {
        $count = count($array);
        $random = random_int(1, $count);
        $index = 1;

        foreach ($array as $line) {
            if ($index === $random) {
                return $this->find($line->id);
            } else {
                $index++;
            }
        }
    }
    public function last(array $array): ? Article
    {
        $count = count($array);
        $index = 1;

        foreach ($array as $line) {
            if ($index === $count) {
                return $this->find($line->id);
            } else {
                $index++;
            }
        }
    }
    public function search(string $searchQ)
    {
        $articles = [];
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare('SELECT * FROM article WHERE titre LIKE :keywords');
        $query->bindValue(':keywords', '%' . $searchQ . '%');
        $query->execute();

        $results = $query->fetchAll();

        if (!$query->rowCount() == 0) {
            foreach ($results as $line) {
                $articles[] = $this->sqlToBlog($line);
            }
        } else {
            echo "mdr tu sais ce que tu cherche ? parce qu'on dirait pas. ";
        }
        return $articles;
    }
}
