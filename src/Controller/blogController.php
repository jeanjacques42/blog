<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\BlogType;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\BlogRepository;


class blogController extends AbstractController
{
    /**
     * @Route ("/", name="blog")
     */
    public function blog(Request $request, BlogRepository $repo)
    {
        $count = count($repo->findAll());
        echo $count;
        dump($repo->findAll());
        return $this->render("blogAffichage.html.twig", [
            "article" => $repo->findAll(),
            "count" => $count
        ]);
    }
    /**
     * @Route ("/remove/{id}", name="remove")
     */
    public function remove(BlogRepository $repo, int $id)
    {
        $article = $repo->find($id);
        $repo->remove($article);
        return $this->redirectToRoute("blog");
    }
    /**
    * @Route ("/add", name="add")
    */
    public function add(Request $request, BlogRepository $repo)
    {
        dump($repo->findAll());

        $article = new Article();
        $form = $this->createForm(BlogType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repo->add($article);
            return $this->redirectToRoute("blog");
        }
        return $this->render("add_article.html.twig", [
            "form" => $form->createView(),
            "article" => $article
        ]);
    }

    /**
     * @Route ("/modify{id}", name="modify")
     */
    public function modify(Request $request, BlogRepository $repo, int $id)
    {
        $article = $repo->find($id);
        $form = $this->createForm(BlogType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repo->update($article);

            return $this->redirectToRoute("blog");
        }
        dump($article);
        return $this->render('add_article.html.twig', [
            'form' => $form->createView(),
            'article' => $article
        ]);
    }


    /**
     * @Route ("/show-article/{id}", name="show_article")
     */
    public function onearticle(BlogRepository $repo, int $id)
    {
        return $this->render("show_article.html.twig", [
            "article" => $repo->find($id)
        ]);
    }
    /**
     * @Route ("/random", name="random")
     */
    public function random(BlogRepository $repo)
    {
        $array = $repo->findAll();
        dump($repo->random($array));
        return $this->render("show_article.html.twig", [
            "article" => $repo->random($array)
        ]);
    }
    /**
     * @Route ("/last", name="last")
     */
    public function last(BlogRepository $repo)
    {
        $array = $repo->findAll();
        dump($repo->last($array));
        return $this->render("show_article.html.twig", [
            "article" => $repo->last($array)
        ]);
    }

    /**
     * @Route ("/search", name="search")
     */
    public function search(BlogRepository $repo, Request $request)
    {
        $articles = $repo->search($request->get('search'));
        dump($articles);
        return $this->render("search.html.twig", [
            'articles' => $articles
        ]);
    }
}
